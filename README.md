# Bizcommerce/theme-maplebear

### Install

```shell
composer config repositories.bizcommerce-theme-maplebear git https://bitbucket.org/biz-commerce/theme-maplebear.git
composer require biz-commerce/theme-maplebear:dev-master
php bin/magento setup:static-content:deploy pt_BR
php bin/magento setup:upgrade
php bin/magento setup:di:compile
```