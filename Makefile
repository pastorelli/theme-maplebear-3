################################################################################
# Main commands
################################################################################

## Starts the app, prep dependencies and warms the cache 
.PHONY: test
test:
	# cd $(MAGE_ROOT)/vendor/biz-commerce/$(DRONE_REPO,,) && \
	# 	yarn install && \
	# 	yarn test

################################################################################
# Utils
################################################################################

## Prepare dependencies
.PHONY: prep
prep:
	## Setup yarn workspace
	echo "preparation"

## Prepare dependencies
.PHONY: start
prep:
	## Setup yarn workspace
	echo "start"